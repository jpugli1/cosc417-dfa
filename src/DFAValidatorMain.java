import java.util.Arrays;
import java.util.Scanner;

public class DFAValidatorMain {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int states, acceptingStates = 0;
		
		// Gets the number of states from the user for the DFA
		System.out.print("Enter the number of states for your DFA: ");
		states = in.nextInt();
		System.out.println();

		// Set arr0 to the array of state transitions with symbol 0
		int[] arr0 = getDFA0(states);
		System.out.println(Arrays.toString(arr0));
		
		// Set arr1 to the array of state transitions with symbol 1
		int[] arr1 = getDFA1(states);
		System.out.println(Arrays.toString(arr1));
		
		// Gets the number of accepting states from the user for the DFA
		System.out.print("Enter the number of accepting states for your DFA: ");
		acceptingStates = in.nextInt();
		System.out.println();
		
		// Set acceptingStatesArr to the user entered accepting states
		int[] acceptingStatesArr = getAcceptingStates(acceptingStates);
		
		// Call to validate function for 3 inputs to see if the input string is accepted or rejected
		for(int i = 0; i < 3; i++) {
			// Gets user input for the input to run against the machine
			System.out.println("Enter the input string to run against the machine: ");
			String input = in.next();
			
			if (validate(input, arr0, arr1, acceptingStatesArr)) {
				System.out.println("ACCEPTED");
			} else {
				System.out.println("REJECTED");
			}
		}
		
		in.close();
	}
	
	// Method to set the transition from one state to another with the symbol 0.
	// The index in the array represents the from state and the value at that index 
	// represents the state which the state goes to with symbol 0.
	public static int[] getDFA0(int states) {
		Scanner in = new Scanner(System.in);
		int numStates = states;
		int[] transition = new int[numStates];
		
		for (int i = 0; i < transition.length; i++) {
			System.out.print("Enter the state M goes to from state " + i + " and symbol 0:");
			transition[i] = in.nextInt();
		}
		
		return transition;
	}
	
	// Method to set the transition from one state to another with the symbol 1.
	// The index in the array represents the from state and the value at that index 
	// represents the state which the state goes to with symbol 1.
	public static int[] getDFA1(int states) {
		Scanner in = new Scanner(System.in);
		int numStates = states;
		int[] transition = new int[numStates];
		
		for (int i = 0; i < transition.length; i++) {
			System.out.print("Enter the state M goes to from state " + i + " and symbol 1:");
			transition[i] = in.nextInt();
		}
		
		return transition;
	}
	
	// Returns an array of all the accepting states entered by the user.
	public static int[] getAcceptingStates(int acceptingStates) {
		Scanner in = new Scanner(System.in);
		int[] accepting = new int[acceptingStates];
		
		for(int i = 0; i < accepting.length; i++) {
			System.out.println("Enter accepting state " + i + ": ");
			accepting[i] = in.nextInt();
		}
		
		return accepting;
	}
	
	// Gets the input string from the user.
	public static String getInputString() {
		Scanner in = new Scanner(System.in);
		String input = "";
		System.out.println("Enter the string to test if the DFA accepts the input: ");
		input = in.next();
		return input;
	}
	
	// Validates if the input string results in a state that is in the accepted states
	public static boolean validate(String input, int[] zeroArray, int[] oneArray, int[] accepting) {
		char c = ' ';
		int currentChar, state = 0;
		boolean accept = false;
		
		// Iterates character by character over the input string
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			currentChar = Character.getNumericValue(c);
			// If character is 0, sets the state to the state 0 transitions to. Same if character is 1, 
			// sets the state to the state 1 transitions to.
			if (currentChar == 0) {
				state = zeroArray[state];
				System.out.println("In state: " + state);
			} else if (currentChar == 1) {
				state = oneArray[state];
				System.out.println("In state: " + state);
			} else {
				System.err.println("Invalid character in input string!!");
				System.exit(0);
			}
		}
		// Checks to see if the final state is in the accepted states.
		for (int i = 0; i < accepting.length; i++) {
			if (accepting[i] == state) {
				accept = true;
			}
		}
		return accept;
	}

}
